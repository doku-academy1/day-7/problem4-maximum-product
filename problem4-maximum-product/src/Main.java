import java.util.*;

public class Main {
    public static void main(String[] args) {
        Integer[] price1 = {25000, 25000, 10000, 14000};
        Integer money1 = 50000;
        System.out.println(Arrays.toString(price1));
        System.out.printf("Money: %d", money1);
        System.out.println();
        System.out.printf("Barang maksimum yang dapat dibeli: %d", maximumProduct(money1, convertArrayToList(price1)));
        System.out.println();

        System.out.println();

        Integer[] price2 = {15000, 10000, 12000, 5000, 3000};
        Integer money2 = 30000;
        System.out.println(Arrays.toString(price2));
        System.out.printf("Money: %d", money2);
        System.out.println();
        System.out.printf("Barang maksimum yang dapat dibeli: %d", maximumProduct(money2, convertArrayToList(price2)));
    }

    public static Integer maximumProduct(Integer money, List<Integer> prices) {
        Integer sum = 0, i , j, max = 0, reduce = money, maksimum = 0;
        Map<Integer, Integer> map = new HashMap<>();

        for (i = 0; i<prices.size(); i++) {

            for (j = i+1; j<prices.size(); j++) {
                sum += prices.get(j);
                if (sum >= money) {
                    max += 1;
                } else {
                    map.put(max, sum-prices.get(j));
                }
            }

            sum = 0;
            max = 0;

            for (int k = prices.size()-1; k>i; k--) {
                reduce -= prices.get(k);
                if (reduce >= 0) {
                    max += 1;
                } else {
                    map.put(max, reduce-prices.get(k));
                }
            }

            sum = 0;
            reduce = money;
        }

        List<Integer> newList = new ArrayList<>();
        for (Integer key: map.keySet()) {
            newList.add(key);
            if (key>maksimum) {
                maksimum = key;
            }
        }

        return maksimum;
    }

    public static List<Integer> convertArrayToList (Integer[] prices) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i<prices.length; i++) {
            list.add(prices[i]);
        }
        return list;
    }

    public static Integer sumAll (List<Integer> prices, Integer index) {
        Integer sum = 0;
        for (int i = prices.size()-1; i!=index; i--) {
            sum += prices.get(i);
        }
        return sum;
    }
}